package prefuse.demos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.MouseEvent;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;

import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.filter.GraphDistanceFilter;
import prefuse.action.layout.graph.ForceDirectedLayout;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.DataColorAction;
import prefuse.action.layout.Layout;
import prefuse.activity.Activity;

import prefuse.controls.ControlAdapter;
import prefuse.controls.DragControl;
import prefuse.controls.FocusControl;
import prefuse.controls.NeighborHighlightControl;
import prefuse.controls.PanControl;
import prefuse.controls.WheelZoomControl;
import prefuse.controls.ZoomControl;
import prefuse.controls.ZoomToFitControl;

import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Table;
import prefuse.data.Tuple;
import prefuse.data.event.TupleSetListener;
import prefuse.data.io.GraphMLReader;
import prefuse.data.tuple.TupleSet;

import prefuse.render.DefaultRendererFactory;
import prefuse.render.LabelRenderer;
import prefuse.render.PolygonRenderer;
import prefuse.render.Renderer;
import prefuse.render.ShapeRenderer;

import prefuse.util.ColorLib;
import prefuse.util.GraphLib;
import prefuse.util.GraphicsLib;
import prefuse.util.display.DisplayLib;
import prefuse.util.display.ItemBoundsListener;
import prefuse.util.force.ForceSimulator;
import prefuse.util.io.IOLib;
import prefuse.util.ui.JForcePanel;
import prefuse.util.ui.JValueSlider;
import prefuse.util.ui.UILib;

import prefuse.visual.AggregateItem;
import prefuse.visual.AggregateTable;
import prefuse.visual.VisualGraph;
import prefuse.visual.VisualItem;

import java.io.*;
import java.util.*;
import prefuse.action.layout.graph.NodeLinkTreeLayout;
import prefuse.render.*;

public class SOMViewFinalIterative extends Display {

    private static final String graph = "graph";
    private static final String nodes = "graph.nodes";
    private static final String edges = "graph.edges";
    private static final String aggr = "aggregates";
    private static final String label = "label";
    private static final String type = "type";
    Graph g;
    
    public int nbOrigNodes = 0;
    public int nbFixedNodes = 0;
    
    public ArrayList<ArrayList<String>> dataTypes = new ArrayList<ArrayList<String>>();  //2012-05-06:MB 2D Array List Test 
    public ArrayList<ArrayList<String>> fixedNodes = new ArrayList<ArrayList<String>>();
    public ArrayList<ArrayList<String>> tokens = new ArrayList<ArrayList<String>>();     
    public ArrayList<String> dTypes = new ArrayList<String>();
   
    public ArrayList<String> allTokens = new ArrayList<String>(); // A flat list of all tokens
    public ArrayList<ArrayList<String>> allTokenData = new ArrayList<ArrayList<String>>(); // Experimental Array
    public ArrayList<Integer> nodeMap = new ArrayList<Integer>(); // A map of how many tokens are at each of the original nodes
    public ArrayList<Integer> typeMap = new ArrayList<Integer>(); // A map of how many data types (nodes) are at each of the original nodes
    public ArrayList<Integer> optTypeMap = new ArrayList<Integer>(); // A map of the data types at each new node
    public ArrayList<Integer> optNodeMap = new ArrayList<Integer>(); // A map of how many tokens are at each of the new nodes
    
    public static final boolean showVisual = false;  //Change this to enable or disable Visualization.
    public static final boolean showIndividualNodes = true; //Change this to enable or disable each token as it's own node. NOTE: NOT FUNCTIONAL YET
    public static final boolean showVisualAggr = false; // Change this to enable or disable aggregation (bubbles around nodes)
    public static final boolean showDetailedNodes = true; //Change this to enable or disable the showing of detailed nodes
    public static final boolean showDetailedONodeLabels = false; // Change this to enabel or disable detailed labelling of original nodes.
    //public static final String filePath = "/root/Dropbox/prefuse-data/";  // Use when working in Linux with Dropbox in the Root Folder    
    //public static String filePath = "Z:/prefuse-data/experiments/1/";  // Use when working in Windows with Dropbox mapped to Z:
    //public static String filePath = "Z:/prefuse-data/";  // Use when working in Windows with Dropbox mapped to Z:    
    public static String filePath = "C:/VLDB";
    
    public String fileNameAugment = "5";
    public String inputFileName = "/Pre/"+ fileNameAugment + "/somGraph_"+fileNameAugment+"grams-1.txt";
    public String outputFileName = "/Post/"+ fileNameAugment + "/somGraph_"+ fileNameAugment + "grams-1-optimized.txt";
    public static Integer iterations = 30; //Set to number of iterations required. Set to 0 for single use.
    
    public SOMViewFinalIterative() {
        //initialize display and data
        
        super(new Visualization());
        int iterCount = 0;
        if (iterations == 0 ) {
            iterCount = 1;         
        } else { 
            iterCount = iterations;
            System.out.println("Setting iterCount to " + iterations + " iterations.");
        }
        
        for (int control = 1; control <= iterCount; control ++) {
            if (iterations > 0 ) {
                inputFileName = "/Pre/"+ fileNameAugment + "/somGraph_"+ fileNameAugment + "grams-" + control + ".txt";
                outputFileName = "/Post/"+ fileNameAugment + "/somGraph_"+ fileNameAugment + "grams-" + control + "-optimized.txt";   
            }            
          System.out.println("Filepath: " + filePath);
          getSOMTypes();
          getSOMGrid();
          commitChanges(outputFileName); //This is the output filename.
        }

        if (showVisual) {
            // set up the renderers
            LabelRenderer nodeRenderer = new LabelRenderer(label);
            nodeRenderer.setRoundedCorner(8, 8);
            Renderer polyR = new PolygonRenderer(Constants.POLY_TYPE_LINE);
            ((PolygonRenderer) polyR).setCurveSlack(0.15f);
            //EdgeRenderer nodeEdges = new EdgeRenfderer();
            DefaultRendererFactory rendererFactory = new DefaultRendererFactory();
            rendererFactory.setDefaultRenderer(nodeRenderer);
            if (showVisualAggr) {
                rendererFactory.add("ingroup('aggregates')", polyR);
            }

            m_vis.setRendererFactory(rendererFactory);

            // set up the visual operators
/*
            int[] palette = new int[]{
                ColorLib.rgba(250, 250, 250, 255), // Color for UNTYPED Data Type                
                ColorLib.rgba(255, 200, 200, 255), // Color for TEXT Data Type
                ColorLib.rgba(200, 200, 255, 255), // Color for INT Data Type
                ColorLib.rgba(230, 255, 230, 255), // Color for REAL Data Type
                ColorLib.rgba(250, 175, 100, 255), // Color for DATE Data Type
                ColorLib.rgba(255, 190, 255, 255), // Color for MIXED Data Type   
                ColorLib.rgba(250, 250, 250, 155), // Color for UNTYPED Data Type                
                ColorLib.rgba(255, 200, 200, 155), // Color for TEXT Data Type
                ColorLib.rgba(200, 200, 255, 155), // Color for INT Data Type
                ColorLib.rgba(230, 255, 230, 155), // Color for REAL Data Type
                ColorLib.rgba(255, 255, 200, 155), // Color for DATE Data Type
                ColorLib.rgba(255, 190, 255, 155) // Color for MIXED Data Type                     
            };
   */         
            int[] palette = new int[]{
                ColorLib.rgba(150, 150, 150, 200), // Color for UNTYPED Data Type                
                ColorLib.rgba(150, 150, 150, 200), // Color for TEXT Data Type
                ColorLib.rgba(150, 150, 150, 200), // Color for INT Data Type
                ColorLib.rgba(150, 150, 150, 200), // Color for REAL Data Type
                ColorLib.rgba(150, 150, 150, 200), // Color for DATE Data Type
                ColorLib.rgba(150, 150, 150, 200), // Color for MIXED Data Type   
                ColorLib.rgba(250, 250, 250, 155), // Color for UNTYPED Data Type                
                ColorLib.rgba(255, 200, 200, 155), // Color for TEXT Data Type
                ColorLib.rgba(200, 200, 255, 155), // Color for INT Data Type
                ColorLib.rgba(230, 255, 230, 155), // Color for REAL Data Type
                ColorLib.rgba(255, 255, 200, 155), // Color for DATE Data Type
                ColorLib.rgba(250, 175, 100, 155) // Color for MIXED Data Type                     
            };            

            int[] palette2 = new int[]{
                ColorLib.rgba(200, 200, 200, 200), // Color for TEXT Data Type
                ColorLib.rgba(0, 0, 0, 128), // Color for INT Data Type          
            };

            // Color Actions
            ColorAction nText = new ColorAction(nodes, VisualItem.TEXTCOLOR);
            nText.setDefaultColor(ColorLib.gray(0));

            ColorAction nStroke = new ColorAction(nodes, VisualItem.STROKECOLOR);
            nStroke.setDefaultColor(ColorLib.gray(0));

            ColorAction edgeStrokes = new ColorAction(edges, VisualItem.STROKECOLOR);
            edgeStrokes.setDefaultColor(ColorLib.gray(0));

            DataColorAction nodeDataColorAction = new DataColorAction(nodes, type, Constants.NUMERICAL, VisualItem.FILLCOLOR, palette);

            // bundle the color actions
            ActionList colors = new ActionList();
            colors.add(nStroke);
            colors.add(nText);
            colors.add(nodeDataColorAction);            
            
            if (showVisualAggr) {
                ColorAction aStroke = new ColorAction(aggr, VisualItem.STROKECOLOR);
                aStroke.setDefaultColor(ColorLib.gray(200));         
                ColorAction aFill = new DataColorAction(aggr, "id", Constants.NOMINAL, VisualItem.FILLCOLOR, ColorLib.getCoolPalette());
                colors.add(aStroke);
                colors.add(aFill);
            }
            // bundle the draw actions
            ActionList draw = new ActionList();
            draw.add(new RepaintAction());
            draw.add(edgeStrokes);

            m_vis.putAction("draw", draw);

            // now create the main layout routine
            ActionList layout = new ActionList(Activity.INFINITY);
            layout.add(colors);
            layout.add(new ForceDirectedLayout(graph, true));
            //layout.add(new NodeLinkTreeLayout(nodes));
            if (showVisualAggr) {            
                layout.add(new AggregateLayout(aggr));
            }
            layout.add(new RepaintAction());
            m_vis.putAction("layout", layout);

            // set up the display
            setSize(800, 800);
            pan(250, 250);
            setHighQuality(true);
            //addControlListener(new AggregateDragControl());
            addControlListener(new ZoomControl());
            addControlListener(new PanControl());
            addControlListener(new FocusControl(1));
            addControlListener(new DragControl());
            addControlListener(new WheelZoomControl());
            addControlListener(new ZoomToFitControl());
            addControlListener(new NeighborHighlightControl());

            // set things running
            m_vis.run("layout");
            m_vis.run("draw");
        }
    }

    /*
     * This method converts the dataType of a token to an integer value Still
     * needs to be made dynamic for more or less data types
     */
    public int dTypeToInt(String inToken) {
        int dTypeInt = 0;

        if (getDataType(inToken).equals("text")) {
            dTypeInt = 1;
        } else if (getDataType(inToken).equals("int")) {
            dTypeInt = 2;
        } else if (getDataType(inToken).equals("real")) {
            dTypeInt = 3;
        } else if (getDataType(inToken).equals("date")) {
            dTypeInt = 4;
        } else if (getDataType(inToken).equals("mixed")) {
            dTypeInt = 5;
        }
        return dTypeInt;
    }
    
    /*
     * This method is used to determine the number of needles in a haystack Or:
     * How many times a specific character (needle) appears in a given string
     * (haystack)
     */
    public static int countOccurrences(String haystack, char needle) {
        int count = 0;
        for (int i = 0; i < haystack.length(); i++) {
            if (haystack.charAt(i) == needle) {
                count++;
            }
        }
        return count;
    }
    /*
     * This method removes the input extension and replaces them with an integer
     */
    public static String fixExtension(String fix, String mode) {        
        String fixed = fix;
        if (mode.equals("min")) {
            fixed = fixed.replace("D.dat", "1");
            fixed = fixed.replace(".txt", "@2");
        }
        if (mode.equals("max")) {
            fixed = fixed.replace("1", "D.dat");
            fixed = fixed.replace("@2", ".txt");        
        }        
        return fixed;
    }
    
    /*
     * This method is used to swap token 0 with token 1 for visualization
     * purposes. This method is also used a second time to revert the order for
     * output to the optimized data file
     */
    public static String fixToken(String fix) {
        String fixed = "";
        if (countOccurrences(fix, '@') == 2) {
            String delims = "@";
            String[] tempFix = fix.split(delims);

            //fixed=tempFix[1]+"@"+tempFix[0].toUpperCase()+"@"+tempFix[2];
            fixed = tempFix[1] + "@" + tempFix[0] + "@" + tempFix[2];
            //fixed = tempFix[1] + "@" + tempFix[0] + "@" + "1";
        } else if (countOccurrences(fix, '.') == 1) {
            String delims = "@";
            String[] tempFix = fix.split(delims);
            String first = "";
            String second = "";
            String work = tempFix[1];
            first = work.substring(0, work.lastIndexOf('.'));
            second = work.substring(work.lastIndexOf('.'), work.length());
            //if (second.equals(null)) {
                //second = ".txt";
            //}
//          fixed = first + "@" + tempFix[0] + "@" + second; // Use this to name files that are NOT .dat files using "column@table@filename.txt NOTE: Filenames appear to be blank so results in column@table.txt
            fixed = first + "@" + tempFix[0] + second; // Use this to name files that are NOT .dat files using "column@filename.txt"
            //fixed = first + "@" + tempFix[0] + "@2";
        }
        return fixed;
    }
    
    
    /*
     * This method is used to return the DataType of a token (parsed between the
     * [ ] characters)
     */
    public String getDataType(String inToken) {
        String gotDataType = "";
        if (!(inToken.equals(""))) {
            gotDataType = inToken.substring(inToken.indexOf("[") + 1, inToken.indexOf("]"));
        }
        return gotDataType;
    }
    /*
     * This method is used to determine and return the number of data types at a node
     */
    public int getNumOfTypesAtNode(int inNode, String nodeArray) {

        ArrayList<ArrayList<String>> tempNodeArray = new ArrayList<ArrayList<String>>();
        
        if (nodeArray.equals("original")) { 
            tempNodeArray = tokens;            
        }
        if (nodeArray.equals("fixed")) { 
            tempNodeArray = fixedNodes;
        }        
        
        int numOfTypesAtNode = 0;
        int tokensAtNode = tempNodeArray.get(inNode).size();
        int[] numOfDTypes = new int[6];        

        for (int j = 0; j < tokensAtNode; j++)
        {
            for (int k = 0; k < numOfDTypes.length ; k++) {
                if ( dTypeToInt(tempNodeArray.get(inNode).get(j)) == k) { 
                    numOfDTypes[k] ++;
                }
            }
        }

        for(int i = 0; i < numOfDTypes.length; ++i) { 
            if(numOfDTypes[i] > 0) { 
                numOfTypesAtNode ++;                
            } 
        } 
        return numOfTypesAtNode;
    }
    
    /*
     * This method is used to determine the major datatype for a Node based on the tokens at that node
     */
    public int getMajorDataType(int inNode, String nodeArray) {
        ArrayList<ArrayList<String>> tempNodeArray = new ArrayList<ArrayList<String>>();
        
        if (nodeArray.equals("original")) { 
            tempNodeArray = tokens;            
        }
        if (nodeArray.equals("fixed")) { 
            tempNodeArray = fixedNodes;
        }
        
        int tokensAtNode = tempNodeArray.get(inNode).size();
        
        int[] numOfDTypes = new int[6]; //5 Data Types + 0 (no type)
       

        for (int j = 0; j < tokensAtNode; j++)
        {

            for (int k = 0; k < numOfDTypes.length; k++) {
                if ( dTypeToInt(tempNodeArray.get(inNode).get(j)) == k) {   
                    numOfDTypes[k] ++; 
                }
            }
        }
        int max = 0; 
        int majorType = 0;
        
        for(int i = 0; i < numOfDTypes.length; ++i) { 
             if(numOfDTypes[i] > max) { 
                max = numOfDTypes[i];
                majorType = i;
            } 
        }
        return majorType;       
    }

     /*
     * This method reorganizes the Array taken from the input file, splits nodes
     * that have mutiple data types and structures a new arrayList in
     * preparation to be output to a new "optimized" file
     */
    public ArrayList<ArrayList<String>> fixOrganization(ArrayList<ArrayList<String>> oldNodes, int aggrCount) {

        ArrayList<ArrayList<String>> newNodes = new ArrayList<ArrayList<String>>();   // The Arrays to contain the new layout     
        int numOfTypes = dTypes.size();                                               // Determine the number of data types to be concerned with
        int[] typeCounts = new int[numOfTypes];                                       // an Array used to count the occurences of data types on a given aggregate

        // Cycle through each Aggregate and determine if it contains more than one data type
        for (int i = 0; i < aggrCount; i++) {  //Should be 54
            for (int k = 0; k < numOfTypes; k++) {
                typeCounts[k] = 0;
            }
            int tokensAtNode = ((ArrayList) oldNodes.get(i)).size();
            
            int aggrNumDataTypes = 0;

            for (int j = 0; j < tokensAtNode; j++) {
                aggrNumDataTypes=0;
                String tokenDataType = getDataType(oldNodes.get(i).get(j));
                 
                for (int k = 0; k < numOfTypes; k++) {
                    if (tokenDataType.equals(dTypes.get(k))) {
                            typeCounts[k]++;
                        }
                    }
                for(int l = 0; l < numOfTypes; l++) { 
                    if(typeCounts[l] > 0) { 
                        aggrNumDataTypes ++;                      
                    } 
                }
            }

            // If an aggregate contains more than 1 data type, split the aggregate by type            
            if (aggrNumDataTypes <= 1) {
                newNodes.add(oldNodes.get(i));
            } else if (aggrNumDataTypes > 1) {

                ArrayList<String> textAtNode = new ArrayList<String>();
                ArrayList<String> intAtNode = new ArrayList<String>();
                ArrayList<String> realAtNode = new ArrayList<String>();
                ArrayList<String> dateAtNode = new ArrayList<String>();
                ArrayList<String> mixedAtNode = new ArrayList<String>();                
                
                for (int k = 0; k < oldNodes.get(i).size(); k++) {
                    if (getDataType(oldNodes.get(i).get(k)).equals("text")) {
                        textAtNode.add(oldNodes.get(i).get(k));
                    } else if (getDataType(oldNodes.get(i).get(k)).equals("int")) {
                        intAtNode.add(oldNodes.get(i).get(k));
                    } else if (getDataType(oldNodes.get(i).get(k)).equals("real")) {
                        realAtNode.add(oldNodes.get(i).get(k));
                    } else if (getDataType(oldNodes.get(i).get(k)).equals("date")) {
                        dateAtNode.add(oldNodes.get(i).get(k));
                    } else if (getDataType(oldNodes.get(i).get(k)).equals("mixed")) {
                        mixedAtNode.add(oldNodes.get(i).get(k));
                    } else {
                        System.out.println("The Node: " + oldNodes.get(i).get(k) + " data type was not identified.");
                    }
                }
                if (textAtNode.size() > 0) {
                    newNodes.add(textAtNode);
                }
                if (intAtNode.size() > 0) {
                    newNodes.add(intAtNode);
                }
                if (realAtNode.size() > 0) {
                    newNodes.add(realAtNode);
                }
                if (dateAtNode.size() > 0) {
                    newNodes.add(dateAtNode);
                }
                if (mixedAtNode.size() > 0) {
                    newNodes.add(mixedAtNode);
                }
            }
        }

        return newNodes;
    }

    /*
     * This method simply creates the new output file for optimization
     */
    public static File CreateFile(String fileName) {
        File oFile = new File(fileName);
        if (!oFile.exists()) {
            try {
                oFile.createNewFile();
                System.out.println("New file " + fileName + " has been created.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return oFile;
    }

    /*
     * This method strips the data type appeneded to the label for output to
     * file
     */
    public String removeDataType(String inToken) {
        String outToken = "";
        if (countOccurrences(inToken, '[') == 1 && countOccurrences(inToken, ']') == 1) {
            String tokenChar = "";
            int i = 0;
            while (!(tokenChar.equals("["))) {
                tokenChar = inToken.substring(i, i + 1);
                if (!(tokenChar.equals("["))) {
                    outToken = outToken + tokenChar;
                }
                i++;
            }
        }
        return outToken;
    }



    /*
     * This method outputs the contents of the restructured Nodes array
     * (newNodes) to a text file It is responsible for calling the appropriate
     * parsers and structurin the output string according to the input file
     * example.
     */
    public void writeNewFile(int newAggrCount, ArrayList<ArrayList<String>> newNodes, File oFile) throws FileNotFoundException, IOException {
        if (oFile == null) {
            throw new IllegalArgumentException("File should not be null.");
        }
        if (!oFile.exists()) {
            throw new FileNotFoundException("File does not exist: " + oFile);
        }
        if (!oFile.isFile()) {
            throw new IllegalArgumentException("Should not be a directory: " + oFile);
        }
        if (!oFile.canWrite()) {
            throw new IllegalArgumentException("File cannot be written: " + oFile);
        }
        //use buffering
        Writer output = new BufferedWriter(new FileWriter(oFile));
        try {
            //FileWriter always assumes default encoding is OK!
            String aContents = "";
            String nodeText = "";
            //Output to new file here by reformatting the Array to a string aContents
            aContents = aContents + "[" + nbFixedNodes + " 1 0]"; //Properly form the header
            for (int i = 0; i < nbFixedNodes; i++) {
                aContents = aContents + "\n" + "{";
                for (int j = 0; j < (newNodes.get(i).size()); j++) {
                    nodeText = newNodes.get(i).get(j);
                    if ((j == newNodes.get(i).size() - 1) && (!(newNodes.get(i).get(j)).equals(""))) {
                        // Try switching removeDataType and FixToken around
                        //System.out.println(fixToken(newNodes.get(i).get(j)));
                        //aContents = aContents + "\'" + removeDataType(fixToken(newNodes.get(i).get(j))) + "\'";
                       
                        //aContents = aContents + "\'" + removeDataType(fixExtension(fixToken(nodeText), "max")) + "\'";
                        aContents = aContents + "\'" + removeDataType(nodeText) + "\'";
                    } else if (!(newNodes.get(i).get(j)).equals("")) {
                        //aContents = aContents + "\'" + removeDataType(fixToken(newNodes.get(i).get(j))) + "\',";

                        //System.out.println("But: " + removeDataType(fixExtension(fixToken(nodeText), "max")));
                        //System.out.println("But: " + removeDataType(nodeText));
                        //aContents = aContents + "\'" + removeDataType(fixExtension(fixToken(nodeText), "max")) + "\',";
                        aContents = aContents + "\'" + removeDataType(nodeText) + "\',";
                    }
                }
                if (!(aContents.substring(aContents.length() - 1, aContents.length()).equals("{"))) {
                    aContents = aContents + ";}";
                } else {
                    aContents = aContents + "empty}";
                }
            }
            System.out.println("Writing contents to file" + aContents);
            output.write(aContents);

        } finally {
            output.close();
        }
    }

    /*
     * This method produces a 2D array containing each Label and the associated
     * data type. The information is pulled from the provided file type.txt the
     * produced array is dataTypes (arrayList) NOTE: More work must be done to
     * remove hardcoding of 5 data types
     */
    public void getSOMTypes() {
        try {
            File file = new File(filePath + "/VLDB_input/type.txt");
            BufferedReader bufRdr2 = new BufferedReader(new FileReader(file));

            String tempTC = bufRdr2.readLine();
            String types = bufRdr2.readLine().toLowerCase();
            String terms = "";

            int typeCount = Integer.parseInt(tempTC.trim());

            StringTokenizer st3 = new StringTokenizer(types, " ");
            String waste = st3.nextToken(); //Skip #n
            for (int i = 0; i < typeCount; i++) {
                dTypes.add(st3.nextElement().toString());
            }

            while ((terms = bufRdr2.readLine()) != null) {
                StringTokenizer st4 = new StringTokenizer(terms);  // Tokenize each record (spaces)
                for (int i = 0; st4.hasMoreTokens(); i++) // If more tokens exist and nodes are not full           
                {
                    // This section needs to be made dynamic for variable numbers of dataTypes.
                    // Currently hardcode to 5 dataTypes
                    // Possibly an arrayList (yes another list)
                    String dt1 = st4.nextToken(); // Add Boolean for Type 1
                    String dt2 = st4.nextToken(); // Add Boolean for Type 2
                    String dt3 = st4.nextToken(); // Add Boolean for Type 3
                    String dt4 = st4.nextToken(); // Add Boolean for Type 4
                    String dt5 = st4.nextToken(); // Add Boolean for Type 5               
                    String tempLabel = st4.nextToken(); // Collect Label                                  

                    ArrayList<String> tempLabelType = new ArrayList<String>();
                    tempLabelType.add(tempLabel);
                    if (dt1.equals("1")) {
                        tempLabelType.add(dTypes.get(0));
                    } else if (dt2.equals("1")) {
                        tempLabelType.add(dTypes.get(1));
                    } else if (dt3.equals("1")) {
                        tempLabelType.add(dTypes.get(2));
                    } else if (dt4.equals("1")) {
                        tempLabelType.add(dTypes.get(3));
                    } else if (dt5.equals("1")) {
                        tempLabelType.add(dTypes.get(4));
                    } else {
                        System.out.println("Error. No data type detected for label: " + tempLabel);
                    }
                    dataTypes.add(tempLabelType);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * This method is repsonsible for reading the data file, breaking it apart
     * into tokens and adding the tokens to an Array that is used to label the
     * nodes.
     */
    private void getSOMGrid() {
        Graph g = new Graph();

        g.addColumn(label, String.class);
        g.addColumn(type, int.class);
             
        try {
            File file = new File(filePath + inputFileName);

            BufferedReader bufRdr = new BufferedReader(new FileReader(file));

            String mapDim = bufRdr.readLine();
            String terms = bufRdr.readLine();

            StringTokenizer st2 = new StringTokenizer(mapDim, "[] "); // Read First Line and parse Map Dimensions
            int m = Integer.parseInt(st2.nextToken()); // Extract m dimension from Token
            int n = Integer.parseInt(st2.nextToken()); // Extract n dimension from Token
            int nbIVperNode = Integer.parseInt(st2.nextToken()); // Extract minimum number of IV per Node from Token

            String temp = "";
            nbOrigNodes = m * n;   // Determine number of nodes (m x n map)
            int nbTokens = 0; // 2012-05-06:MB Remove after testing
            String[] termT = new String[nbOrigNodes];  //Create string array of dimension nbOrigNodes
     

            //extracting Node Names.
            if (nbIVperNode > 0) {   // Nodes can not be empty;
                StringTokenizer st = new StringTokenizer(terms, "{}'");
                for (int i = 0; st.hasMoreTokens(); i++) {
                    // get next token and store it in the array
                    for (int j = 0; j < nbIVperNode; j++) {
                        temp += st.nextToken() + "\n";
                    }
                    termT[i] = temp.trim();
                    temp = "";
                }
            } else {
                int matchCount = 0; // Used for diagnostics on String Matching between tokens and dataTypes
                StringTokenizer st = new StringTokenizer(terms, "{}"); // Tokenize strings using {} separators
                for (int i = 0; st.hasMoreTokens() && i < nbOrigNodes; i++) {  // If more tokens exist and nodes are not full

                    temp = st.nextToken();

                    if (temp.trim().equals("'") || temp.trim().equals("''")) { // If temp string is blank
                        temp = st.nextToken();
                    }
                    temp = temp.replace("'", ""); // remove all apostrophes
                    temp = temp.replace(";", ""); // remove all semicolons
                    if (temp.trim().equals("empty")) {  // if temp equals "empty"
                        temp = ""; // make temp blank
                    }
                    String delims = "[,]";
                    String[] tempSubTokens = temp.split(delims); // Breaks the node into separate tokens and stores them in the tempSubTokens array

                    for (int j = 0; j < tempSubTokens.length; j++) {
                        for (int k = 0; k < dataTypes.size(); k++) {
                            if (tempSubTokens[j].trim().toLowerCase().equals(dataTypes.get(k).get(0).trim().toLowerCase())) {
                                //tempSubTokens[j] = fixExtension(fixToken(tempSubTokens[j]), "min");
                                //tempSubTokens[j] = fixToken(tempSubTokens[j]);
                                tempSubTokens[j] = tempSubTokens[j];                                
                                tempSubTokens[j] = tempSubTokens[j] + "[" + dataTypes.get(k).get(1) + "]";//Add data type for Token to tempSubTokens. Also, reformat the Token
                                matchCount++;
                            }
                        }
                    }

                    if (showIndividualNodes) { 
                        //Rearrange tempSubTokens[] here

                    }                    
                    
                    ArrayList<String> subTokens = new ArrayList(Arrays.asList(tempSubTokens));

                    tokens.add(subTokens);                
                     
                    nbTokens = nbTokens + subTokens.size();

                    termT[i] = temp.trim();  // Add trimmed temp string to Node Array termT[i]
                    temp = ""; // Clear contents of temp                   
                }
            }

            
            fixedNodes = fixOrganization(tokens, nbOrigNodes);
            nbFixedNodes = fixedNodes.size();
            System.out.println("Old Node Count:" + nbOrigNodes);
            System.out.println("New Node Count:" + nbFixedNodes); 
            
                      
            //Prepare alLTokens Array
            for (int i=0; i < nbFixedNodes; i++) {
                int tokensAtNode = fixedNodes.get(i).size();
                for (int j=0; j < tokensAtNode; j++) {
                    allTokens.add(fixedNodes.get(i).get(j));                
                }
            }
            
            //Prepare nodeMap Array
            int tempTokenCount2 = 0;
            for (int i=0; i < nbOrigNodes; i ++) {
                
                int tokensAtNode = 0;

                    tokensAtNode = tokens.get(i).size();
                    tempTokenCount2 = tempTokenCount2 + tokensAtNode;
              
                nodeMap.add(tokensAtNode);
            }
            
            //Prepare typeMap Array

            for (int i=0; i < nbOrigNodes; i ++) {
                
                int typesAtNode = getNumOfTypesAtNode( i, "original");
              
                typeMap.add(typesAtNode);
            }

            
            //Prepare optTypeMap Array
            for (int i=0; i < nbFixedNodes; i++) {
                
                int type = getMajorDataType(i, "fixed");
                
                optTypeMap.add(type);
            }
            
            // Prepare optNodeMap Array
            int tempTokenCount3 = 0;            
            for (int i=0; i < nbFixedNodes; i ++) {
                
                int tokensAtNode = 0;

                //if (!(fixedNodes.get(i).get(0).equals(""))) {
                    tokensAtNode = fixedNodes.get(i).size();
                    tempTokenCount3 = tempTokenCount3 + tokensAtNode;
                //}                
                optNodeMap.add(tokensAtNode);
                            
            }            
         
            int visualNodes = fixedNodes.size() + nbOrigNodes;
            Node[] nodesG = new Node[visualNodes];  //This makes sure there is room for m * n nodes + each unique node (for labels)
            // This if for the revised visualization:
            
            // This Section produces the Data Typed m x n grid of nodes (Parent Nodes)
            for (int i = 0; i < nbOrigNodes; i++) {
                nodesG[i] = g.addNode();
                int gotMajorDataType = getMajorDataType(i, "original");
                String thisNodeType = "";
                if (gotMajorDataType == 1) { thisNodeType = "text"; }
                if (gotMajorDataType == 2) { thisNodeType = "int"; }
                if (gotMajorDataType == 3) { thisNodeType = "real"; }
                if (gotMajorDataType == 4) { thisNodeType = "date"; }
                if (gotMajorDataType == 5) { thisNodeType = "mixed"; }
                if (showDetailedONodeLabels) {
                    nodesG[i].setString(label, thisNodeType + " " + i + " " + getNumOfTypesAtNode(i, "original"));
                }
                else {
                    //nodesG[i].setString(label, "" + i + ".");
                    nodesG[i].setString(label, "");
                }
                nodesG[i].setInt(type, gotMajorDataType);                
                if (i >= n) {
                    g.addEdge(nodesG[i - n], nodesG[i]);
                }
                if (i % n != 0) {
                    g.addEdge(nodesG[i - 1], nodesG[i]);
                }
            }
            //************************************************************************
            // Here I need to find a way to add the proper nodes from Fixed to the graph, but also
            // maintain the proper association with the original node
            // I will add the nodes from the allTokens array according to the nodeMap array
            
            int tokenControl = 0; //Used decrementally to control the token position
            int offSet = 0;
            
            int nodesToAdd = nodeMap.size();
            String tempLabel = "";
            for (int i = 0; i < nodesToAdd; ++i) {
                
                // for each node in nodeMap, get the corresponding number of types, and add a node.
                // Then, get the data type from optTypeMap
                
                int typesToAdd = typeMap.get(i);
                for (int j=0; j < typesToAdd; j++) {
                    nodesG[nbOrigNodes + i] = g.addNode();
                    if (showDetailedNodes) {
                        tempLabel = "";
                        // if there are no data types at the node, set the label to "empty" and add one token.
                        if (optTypeMap.get(i+offSet) == 0) {
                            tempLabel = null;
                            nodesG[nbOrigNodes + i].setString(label, tempLabel);
                            
                            tokenControl ++;
                        // Otherwise,     
                        } else {
                            // For every token at the new node
                            // Get number of tokens at new node (nbOrigNodes + i + offSet + j)
                            // Add that number of tokens to the 
                            int numberOfExpectedTokens = optNodeMap.get(i + offSet + j);
                                                        
                            for (int k=0; k < numberOfExpectedTokens; k++) {
                                tempLabel = tempLabel + removeDataType(allTokens.get(tokenControl));
                                if ((numberOfExpectedTokens-k) > 1) {
                                    tempLabel = tempLabel + "\n";
                                }
                                tokenControl ++;
                            }
                  
                            //nodesG[nbOrigNodes + i].setString(label, "Node " + (nbOrigNodes + i + offSet + j) + " | " + numberOfExpectedTokens + "\n" + tempLabel);
                            nodesG[nbOrigNodes + i].setString(label, tempLabel);
                        }
                    } else {
                        nodesG[nbOrigNodes + i].setString(label, "" + (i+j) + ".");
                        tokenControl ++;
                    }
                    nodesG[nbOrigNodes + i].setInt(type, optTypeMap.get(i+j+offSet)+6);
                    
                    if (!(optTypeMap.get(i+offSet) == 0)) {
                        g.addEdge(nodesG[nbOrigNodes + i], nodesG[i]);   
                    }
                    
                }
                offSet = offSet+(typesToAdd-1);    
            }
             //************************************************************************           

            if (showVisual) {
                // add visual 
                VisualGraph vg = m_vis.addGraph(graph, g);
                m_vis.setInteractive(edges, null, false);
                //m_vis.setValue(nodes, null, VisualItem.SHAPE, new Integer(Constants.SHAPE_ELLIPSE));

                if (showVisualAggr) {            
                    AggregateTable at = m_vis.addAggregates(aggr);
                    at.addColumn(VisualItem.POLYGON, float[].class);
                    at.addColumn("id", int.class);
                

                // add nodes to aggregates
                // create an aggregate for each node and add one node to it
                Iterator nodes = vg.nodes();
                for (int i = 0; i < (nbOrigNodes); ++i) {
                    AggregateItem aitem = (AggregateItem) at.addItem();
                    aitem.setInt("id", i);
                    for (int j = 0; j < ((ArrayList) tokens.get(i)).size(); ++j) {
                        aitem.addItem((VisualItem) nodes.next()); //Adds nodes to Aggregates Working but aggregates are growing as nodes expand.. need to bind nodes

                    }
                    
                    //Add edges binding first node in each aggregate (original grid)  
                    if (i >= n) {
                        //g.addEdge(nodesG[i - n], nodesG[i]);
                    }
                    if (i % n != 0) {
                       // g.addEdge(nodesG[i - 1], nodesG[i]);
                    }
                }
                }

                /*
                 * //This section is responsible for adding the edges connecting
                 * each node to the previous node within the same aggregate //It
                 * has been muted and copied for use in trying the snowflake
                 * concept //Add Edges to nodes within an aggregate int
                 * currentToken=0; for (int i = 0; i < (nbOrigNodes); i++) { int
                 * tokensAtNode = ((ArrayList) tokens.get(i)).size(); int
                 * nodeToken = 0; int firstNodeToken = currentToken;
                 *
                 * for (int j = 0; j < (tokensAtNode); j++) {
                 *
                 * if (j > 0) { g.addEdge(g.getNode(currentToken),
                 * g.getNode(currentToken-1)); } currentToken ++; } //Add edges
                 * binding first node in each aggregate (original grid) if ( i
                 * >= n ) g.addEdge(nodesG[i-n], nodesG[i]); if ( i % n != 0)
                 * g.addEdge(nodesG[i-1], nodesG[i]); }
                 */

                // When working, this section will add the edges in 'SnowFlake' pattern
                //Add Edges to nodes within an aggregate
                int currentToken = 0;
                for (int i = 0; i < (nbOrigNodes); i++) {
                    int tokensAtNode = ((ArrayList) tokens.get(i)).size();
                    int nodeToken = 0;
                    int firstNodeToken = currentToken;
                    
                    if (showDetailedNodes) {
                        for (int j = 0; j < tokensAtNode; j++) {
                            //g.addEdge(g.getNode(firstNodeToken), g.getNode(currentToken));
                            currentToken++;
                        }
                    }
                    //Add edges binding first node in each aggregate (original grid)  
                    if (i >= n) {
                       // g.addEdge(nodesG[i - n], nodesG[i]);
                    }
                    if (i % n != 0) {
                        //g.addEdge(nodesG[i - 1], nodesG[i]);
                    }
                }

            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void commitChanges(String fileName) {
        try {

            File tFile = CreateFile(filePath + fileName);
            writeNewFile(fixedNodes.size(), fixedNodes, tFile);
            System.out.println("The file " + filePath + fileName + " has been written.");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setGraph(Graph g, String label) {
        // update labeling
        DefaultRendererFactory drf = (DefaultRendererFactory) m_vis.getRendererFactory();
        ((LabelRenderer) drf.getDefaultRenderer()).setTextField(label);

        // update graph
        m_vis.removeGroup(graph);
        VisualGraph vg = m_vis.addGraph(graph, g);
        m_vis.setValue(edges, null, VisualItem.INTERACTIVE, Boolean.FALSE);
        VisualItem f = (VisualItem) vg.getNode(0);
        m_vis.getGroup(Visualization.FOCUS_ITEMS).setTuple(f);
        f.setFixed(false);
    }

    public static JFrame demo() {
        //final GraphView view = new GraphView(g, label);       
        SOMViewFinalIterative psv = new SOMViewFinalIterative();
        // launch window
        JFrame frame = new JFrame("p r e f u s e  |  S O M V i e w");
        frame.getContentPane().add(psv);
        frame.pack();
        //frame.setContentPane(view);
        //frame.pack();
        //frame.setVisible(true);
        return frame;
    }

    //Main and demo methods
    public static void main(String[] args) {

        UILib.setPlatformLookAndFeel();

        JFrame frame = demo();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if (showVisual) {
            frame.setVisible(true);
        }
    }

    /**
     * Swing menu action that loads a graph into the graph viewer.
     */
    public abstract static class GraphMenuAction extends AbstractAction {

        private GraphView m_view;

        public GraphMenuAction(String name, String accel, GraphView view) {
            m_view = view;
            this.putValue(AbstractAction.NAME, name);
            this.putValue(AbstractAction.ACCELERATOR_KEY,
                    KeyStroke.getKeyStroke(accel));
        }

        public void actionPerformed(ActionEvent e) {
            m_view.setGraph(getGraph(), "label");
        }
        
        protected abstract Graph getGraph();
    }
} // end of class GraphView
