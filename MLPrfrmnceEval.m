function MLPrfrmnceEval(mtrx, step)
global y;
sz=size(mtrx,2);

accuracy = 0;
recall = 0;
precision=0;
specificity = 0;
f_measure = 0;
entropy = 0;
yoden = 0;
likelihoodPlus=0;
likelihoodMinus=0;
DP = 0;
 nbDocInClasses = 0;
for i=1:sz,
    
    nbDocInClasses=nbDocInClasses+mtrx(i).rate;
    accuracy = accuracy+ (1*((mtrx(i).tp + mtrx(i).tn)/(mtrx(i).tp+mtrx(i).fp+mtrx(i).fn+mtrx(i).tn)));
    
    %Recall
    recallTemp = (mtrx(i).tp/(mtrx(i).tp+mtrx(i).fn));
    recall=recall +(mtrx(i).rate*recallTemp);
    
    %Precision
    precTemp =  mtrx(i).tp/(mtrx(i).tp+mtrx(i).fp);
    precision = precision + (mtrx(i).rate*precTemp);
    
    specTemp = mtrx(i).tn/(mtrx(i).fp+mtrx(i).tn);
    specificity =specificity + mtrx(i).rate*specTemp;
    
    
 
    
end;

accuracy=(accuracy*100)/nbDocInClasses;
%fprintf( 'Accuracy: %f \n',accuracy);
fprintf( 'precision: %f \n',precision*100); 
fprintf( 'recall: %f \n',recall*100);
fprintf( 'F-MEasure: %f \n',((2*recall*precision)/(recall+precision))*100);

 %WRITING RES
%fid = fopen(sprintf('C:\\VLDB\\Pre\\%d\\MAT_RES-%d.txt',y,y), 'at');
fid = fopen('C:\VLDB\Test\MAT_RES.txt', 'at');
fprintf(fid, 'precision: %f \n',precision*100); 
fprintf(fid, 'recall: %f \n',recall*100);
fprintf(fid, 'F-MEasure: %f \n',((2*recall*precision)/(recall+precision))*100);

PerfMat=[step (precision*100) (recall*100) ((2*recall*precision)/(recall+precision))*100];

if step == 1 
%dlmwrite(sprintf('C:\\VLDB\\Pre\\%d\\firstset-pre-%d.csv',y,y), PerfMat, '-append', 'precision', 9);
dlmwrite('C:\VLDB\Test\firstset-post-test.csv', PerfMat, '-append', 'precision', 9);
end

if step == 2
%dlmwrite(sprintf('C:\\VLDB\\Pre\\%d\\secondset-pre-%d.csv',y,y), PerfMat, '-append', 'precision', 9);
dlmwrite('C:\VLDB\Test\secondset-post-test.csv', PerfMat, '-append', 'precision', 9);
end

